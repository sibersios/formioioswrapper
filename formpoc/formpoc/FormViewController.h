//
//  FormViewController.h
//  formpoc
//
//  Created by Sultanbekov Rasul on 9/14/16.
//  Copyright © 2016 Sibers. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kFormKey;
extern NSString * const kDataKey;

@class FormViewController;

@protocol FormViewControllerDelegate <NSObject>

- (void)formViewController:(FormViewController *)formViewController didFetchData:(NSDictionary *)data;

@end


@interface FormViewController : UIViewController

@property (nonatomic, weak) id<FormViewControllerDelegate> delegate;

@property (nonatomic, strong) NSDictionary *formData;

@end
