webpackJsonp([2],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	__webpack_require__(122);
	
	var _app = __webpack_require__(119);
	
	var _app2 = _interopRequireDefault(_app);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var MODULE_NAME = 'app';
	
	function setupWebViewJavascriptBridge(callback) {
	  if (window.WebViewJavascriptBridge) {
	    return callback(window.WebViewJavascriptBridge);
	  }
	
	  if (window.WVJBCallbacks) {
	    return window.WVJBCallbacks.push(callback);
	  }
	
	  window.WVJBCallbacks = [callback];
	
	  var WVJBIframe = document.createElement('iframe');
	  WVJBIframe.style.display = 'none';
	  WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
	  document.documentElement.appendChild(WVJBIframe);
	
	  setTimeout(function () {
	    return document.documentElement.removeChild(WVJBIframe);
	  });
	
	  return window.WVJBCallbacks;
	}
	
	var bootstrap = function bootstrap() {
	  var app = angular.module(MODULE_NAME, ['ui.router', 'ui.bootstrap', 'formio']).config(_app2.default);
	
	  setupWebViewJavascriptBridge(function (bridge) {
	    app.constant('WVJBridge', bridge);
	
	    angular.bootstrap(document, [MODULE_NAME]);
	  });
	};
	
	document.addEventListener('DOMContentLoaded', bootstrap, false);
	
	exports.default = MODULE_NAME;

/***/ },

/***/ 119:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _controllers = __webpack_require__(121);
	
	exports.default = ['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
	  $urlRouterProvider.otherwise('/');
	
	  $stateProvider.state('home', {
	    url: '/',
	    controller: _controllers.FormController,
	    template: __webpack_require__(340)
	  });
	}];

/***/ },

/***/ 120:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var FormController = exports.FormController = ['$rootScope', '$scope', '$state', '$timeout', '$injector', function ($rootScope, $scope, $state, $timeout, $injector) {
	  var WVJBridge = $injector.get('WVJBridge');
	
	  var formIO = {
	    data: null,
	    form: null
	  };
	
	  $scope.formIO = formIO;
	
	  WVJBridge.callHandler('getFormData', function (_ref) {
	    var form = _ref.form;
	    var data = _ref.data;
	
	    formIO.data = data;
	    formIO.form = form;
	
	    var unsubscribe = $scope.$on('formLoad', function (_ref2) {
	      var formIOScope = _ref2.targetScope;
	
	      formIOScope.submission.data = data;
	
	      unsubscribe();
	    });
	
	    $scope.$apply();
	  });
	
	  WVJBridge.registerHandler('submitForm', function (nil, callback) {
	    var formElement = angular.element('.form-wrapper form');
	
	    if (formElement.length) {
	      (function () {
	        var unsubscribe = $scope.$on('formSubmission', function (e, _ref3) {
	          var data = _ref3.data;
	
	          e.preventDefault();
	
	          unsubscribe();
	
	          callback(data);
	        });
	
	        formElement.one('submit', function (e) {
	          e.preventDefault();
	        });
	
	        formElement.trigger('submit');
	      })();
	    }
	  });
	}];

/***/ },

/***/ 121:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _formController = __webpack_require__(120);
	
	Object.keys(_formController).forEach(function (key) {
	  if (key === "default" || key === "__esModule") return;
	  Object.defineProperty(exports, key, {
	    enumerable: true,
	    get: function get() {
	      return _formController[key];
	    }
	  });
	});

/***/ },

/***/ 122:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 340:
/***/ function(module, exports) {

	module.exports = "<div class=\"form-wrapper\" ng-if=\"formIO.form\">\n  <formio form=\"formIO.form\"></formio>\n</div>\n"

/***/ }

});
//# sourceMappingURL=app.js.map