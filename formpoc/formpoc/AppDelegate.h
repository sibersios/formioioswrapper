//
//  AppDelegate.h
//  formpoc
//
//  Created by Sultanbekov Rasul on 9/14/16.
//  Copyright © 2016 Sibers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

