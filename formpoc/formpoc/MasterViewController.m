//
//  MasterViewController.m
//  formpoc
//
//  Created by Sultanbekov Rasul on 9/14/16.
//  Copyright © 2016 Sibers. All rights reserved.
//

#import "MasterViewController.h"

#import "FormViewController.h"

@interface MasterViewController () <FormViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextView *textView;

@property (nonatomic, strong) NSDictionary *form;
@property (nonatomic, strong) NSDictionary *data;

@end

@implementation MasterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.form = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"form" ofType:@"json"]]
                                                options:0
                                                  error:nil];
}

- (IBAction)formTouchUpInside:(id)sender
{
    FormViewController *formViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FormViewController"];
    formViewController.delegate = self;
    formViewController.formData = [self buildFormData];
    [self presentViewController:formViewController animated:YES completion:nil];
}

- (IBAction)uploadTouchUpInside:(id)sender
{

}

- (void)setData:(NSDictionary *)data
{
    _data = data;
    self.textView.text = _data.description;
}

- (NSDictionary *)buildFormData
{
    return @{ kFormKey : self.form,
              kDataKey : self.data ?: [NSDictionary dictionary] };
}

#pragma mark FormViewControllerDelegate

- (void)formViewController:(FormViewController *)formViewController didFetchData:(NSDictionary *)data
{
    self.data = data;
    [formViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
