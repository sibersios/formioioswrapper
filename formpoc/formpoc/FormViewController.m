//
//  FormViewController.m
//  formpoc
//
//  Created by Sultanbekov Rasul on 9/14/16.
//  Copyright © 2016 Sibers. All rights reserved.
//

#import "FormViewController.h"

#import <Cordova/CDVViewController.h>
#import "WebViewJavascriptBridge.h"

NSString * const kFormKey = @"form";
NSString * const kDataKey = @"data";

@interface FormViewController ()

@property (nonatomic, weak) IBOutlet UIView *cordovaContainerView;

@property (nonatomic, strong) CDVViewController *cordovaViewController;
@property (nonatomic, strong) WebViewJavascriptBridge *bridge;

@end

@implementation FormViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.cordovaViewController = [CDVViewController new];
    self.cordovaViewController.wwwFolderName = @"www";
    self.cordovaViewController.startPage = @"index.html";

    [self.cordovaViewController willMoveToParentViewController:self];
    [self addChildViewController:self.cordovaViewController];
    [self.cordovaContainerView addSubview:self.cordovaViewController.view];
    self.cordovaViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *views = @{ @"cordova" : self.cordovaViewController.view };
    [self.cordovaContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cordova]|"
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:views]];
    [self.cordovaContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cordova]|"
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:views]];
    [self.cordovaViewController didMoveToParentViewController:self];


    [WebViewJavascriptBridge enableLogging];
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:(UIWebView *)self.cordovaViewController.webView];

    [self.bridge registerHandler:@"getFormData" handler:^(id data, WVJBResponseCallback responseCallback) {
        responseCallback(self.formData);
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.bridge callHandler:@"fillForm" data:self.formData responseCallback:^(id responseData) {
        // Not used
        // Can be removed.
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self releaseReferences];
}

- (void)dealloc
{
    NSLog(@"form view controller deallocated");
}

- (IBAction)submitTouchUpInside:(id)sender
{
    [self.bridge callHandler:@"submitForm" data:nil responseCallback:^(id responseData) {
        if ([responseData isKindOfClass:NSDictionary.class]) {
            if ([self.delegate respondsToSelector:@selector(formViewController:didFetchData:)]) {
                [self.delegate formViewController:self didFetchData:responseData];
            }
        }
    }];
}

- (IBAction)cancelTouchUpInside:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)releaseReferences
{
    self.cordovaViewController = nil;
    self.bridge = nil;
}

@end
